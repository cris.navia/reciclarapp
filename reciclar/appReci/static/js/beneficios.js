$(document).ready(function() { 
    $("[id='contImgBenef']").hover(function() { 
      
      $($(this).find("#imgLogo")).css('display', 'none');
      $($(this).children("#descBenef")).css('display', 'initial');
      $($(this)).css('border-style', 'dotted');
      $($(this)).css('box-shadow', '0 15px 25px rgba(0, 0, 0, .5)');
    }, function() { 
      $($(this).find("#imgLogo")).css('display', 'initial');
      $($(this).children("#descBenef")).css('display', 'none');
      $($(this)).css('border-style', 'none');
      $($(this)).css('box-shadow', 'none');
    }); 
  });