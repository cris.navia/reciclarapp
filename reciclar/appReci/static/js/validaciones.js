/*CONTACTO*/
var boolNombre = false;
var boolCorreo = false;
var boolNum = false;
var boolCom = false;


$('#contact_name').on('input', function () {
    var txtNombre = $('#contact_name').val();
    if (txtNombre.length < 10) {

        $('#contact_name').css('border-bottom', '1px solid red');
        boolNombre = false;

    } else {
        $('#contact_name').css('border-bottom', '1px solid white');
        boolNombre = true;
    }


});

$('#contact_email').on('input', function () {
    var regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    var txtEmail = $('#contact_email').val();

    if (!regex.test(txtEmail)) {
        $('#contact_email').css('border-bottom', '1px solid red');
        boolCorreo = false;
    } else {
        $('#contact_email').css('border-bottom', '1px solid white');
        boolCorreo = true;

    }
});

$('#contact_phone').on('input', function () {
    var txtPhone = $('#contact_phone').val();
    if ((!isNaN(txtPhone)) && (txtPhone.length == 8)) {
        $('#contact_phone').css('border-bottom', '1px solid white');
        boolNum = true;

    } else {
        $('#contact_phone').css('border-bottom', '1px solid red');
        boolNum = false;
    }

});

$('#contact_message').on('input', function () {
    var txtMsg = $('#contact_message').val();
    if (txtMsg.length >= 100 && txtMsg.length <= 300) {

        $('#contact_message').css('border-bottom', '1px solid white');
        boolCom = true;

    } else {
        $('#contact_message').css('border-bottom', '1px solid red');
        boolCom = false;
    }


});

$('#inputSubmit').on('click', function () {
    var listo = true;

    

    if(!boolNombre){
        $('#spanName').removeClass('error').addClass('error_show');
        listo = false;
    }
    if(!boolCorreo){
        $('#spanEmail').removeClass('error').addClass('error_show');
        listo = false;
    }
    if(!boolNum){
        $('#spanPhone').removeClass('error').addClass('error_show');
        listo = false;
    }
    if(!boolCom){
        $('#spanComentary').removeClass('error').addClass('error_show');
        listo = false;
    }

    if(!listo){
        event.preventDefault();
        $('#contact_name, #contact_email, #contact_phone, #contact_message').css('border-bottom', '1px solid red');
    }
     
});

/*CONTACTO*/
