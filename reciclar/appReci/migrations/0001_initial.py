# Generated by Django 2.2.6 on 2019-10-29 21:04

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Beneficios',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('CostoPuntaje', models.IntegerField()),
                ('Descripcion', models.CharField(max_length=200)),
                ('Disponible', models.CharField(choices=[('S', 'Activo'), ('N', 'Caducado')], max_length=1)),
            ],
        ),
        migrations.CreateModel(
            name='TipoReciclaje',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Descripcion', models.CharField(max_length=200)),
                ('PuntajeTipo', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Nombre', models.CharField(max_length=20)),
                ('Apellido', models.CharField(max_length=20)),
                ('Telefono', models.CharField(max_length=9)),
                ('Comuna', models.CharField(max_length=9)),
            ],
        ),
        migrations.CreateModel(
            name='PuntosReciclaje',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Calle', models.CharField(max_length=200)),
                ('Numero', models.IntegerField()),
                ('FechaCreacion', models.DateTimeField(default=django.utils.timezone.now)),
                ('TipoReciclaje', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='appReci.TipoReciclaje')),
            ],
        ),
        migrations.CreateModel(
            name='Perfil',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('User', models.CharField(max_length=8)),
                ('Password', models.CharField(max_length=8)),
                ('Puntaje', models.IntegerField()),
                ('Usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='appReci.Usuario')),
            ],
        ),
        migrations.CreateModel(
            name='HistorialReciclaje',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Fecha', models.DateTimeField(default=django.utils.timezone.now)),
                ('PuntosReciclados', models.IntegerField()),
                ('Descripcion', models.CharField(max_length=200)),
                ('Perfil', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='appReci.Perfil')),
                ('PuntoReciclaje', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='appReci.PuntosReciclaje')),
            ],
        ),
        migrations.CreateModel(
            name='HistorialPuntaje',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Fecha', models.DateTimeField(default=django.utils.timezone.now)),
                ('Descripcion', models.CharField(max_length=200)),
                ('TotalPuntaje', models.IntegerField()),
                ('Perfil', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='appReci.Perfil')),
            ],
        ),
        migrations.CreateModel(
            name='Empresa',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Telefono', models.CharField(max_length=8)),
                ('NombreContacto', models.CharField(max_length=20)),
                ('Direccion', models.CharField(max_length=50)),
                ('TipoReciclaje', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='appReci.TipoReciclaje')),
            ],
        ),
        migrations.CreateModel(
            name='Canje',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Fecha', models.DateTimeField(default=django.utils.timezone.now)),
                ('Beneficios', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='appReci.Beneficios')),
                ('Perfil', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='appReci.Perfil')),
            ],
        ),
        migrations.AddField(
            model_name='beneficios',
            name='Empresa',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='appReci.Empresa'),
        ),
    ]
