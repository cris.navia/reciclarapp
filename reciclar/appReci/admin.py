from django.contrib import admin
from .models import *

# Register your models here.

admin.site.register(Perfil)
admin.site.register(Usuario)
admin.site.register(TipoReciclaje)
admin.site.register(PuntosReciclaje)
admin.site.register(HistorialReciclaje)
admin.site.register(HistorialPuntaje)
admin.site.register(Empresa)
admin.site.register(Beneficios)
admin.site.register(Canje)