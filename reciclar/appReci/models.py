from django.db import models
from django.utils import timezone

# Create your models here.
class TipoReciclaje(models.Model):
    Descripcion = models.CharField(max_length=200)
    PuntajeTipo = models.IntegerField()


    def __str__(self):
        return (self.Descripcion, self.PuntajeTipo)

class PuntosReciclaje(models.Model):
    Calle = models.CharField(max_length=200)
    Numero = models.IntegerField()
    FechaCreacion = models.DateTimeField(default=timezone.now)
    TipoReciclaje =  models.ForeignKey(TipoReciclaje, null=False, blank=False, on_delete=models.CASCADE)

    def __str__(self):
        return (self.Calle, self.Numero, self.TipoReciclaje)

class Usuario(models.Model):
    Nombre = models.CharField(max_length=20)
    Apellido = models.CharField(max_length=20)
    Telefono = models.CharField(max_length=9)
    Comuna = models.CharField(max_length=9)
    
    def NombreCompleto(self):
        cadena = "(0) (1)"
        return cadena.format(self.Apellido, self.Nombre)

class Perfil(models.Model):
    User = models.CharField(max_length=8)
    Password = models.CharField(max_length=8)
    Puntaje = models.IntegerField()
    Usuario = models.ForeignKey(Usuario, null=False, blank=False, on_delete=models.CASCADE)
    
    def __str__(self):
        return (self.User, self.Password, self.Puntaje)

class HistorialReciclaje(models.Model):
    Fecha = models.DateTimeField(default=timezone.now)
    Perfil =  models.ForeignKey(Perfil, null=False, blank=False, on_delete=models.CASCADE)
    PuntoReciclaje =  models.ForeignKey(PuntosReciclaje, null=False, blank=False, on_delete=models.CASCADE)
    PuntosReciclados = models.IntegerField()
    Descripcion = models.CharField(max_length=200)

    def __str__(self):
        return(self.Perfil, self.Fecha, self.PuntoReciclaje, self.PuntosReciclados, self.Descripcion)

class HistorialPuntaje(models.Model):
    Perfil =  models.ForeignKey(Perfil, null=False, blank=False, on_delete=models.CASCADE)
    Fecha = models.DateTimeField(default=timezone.now)
    Descripcion = models.CharField(max_length=200)
    TotalPuntaje = models.IntegerField()
    
    def __str__(self):
        return(self.Perfil, self.Fecha, self.Descripcion, self.TotalPuntaje)

class Empresa(models.Model):
    TipoReciclaje =  models.ForeignKey(TipoReciclaje, null=False, blank=False, on_delete=models.CASCADE)
    Telefono = models.CharField(max_length=8)
    NombreContacto = models.CharField(max_length=20)
    Direccion = models.CharField(max_length=50)

    def __str__(self):
        return(self.TipoReciclaje, self.Telefono, self.NombreContacto, self.Direccion)

class Beneficios(models.Model):
    Empresa =  models.ForeignKey(Empresa, null=False, blank=False, on_delete=models.CASCADE)
    CostoPuntaje = models.IntegerField()
    Descripcion = models.CharField(max_length=200)
    ACTIVO = (('S', 'Activo'), ('N', 'Caducado'))
    Disponible = models.CharField(max_length=1, choices=ACTIVO)

class Canje(models.Model):
    Perfil =  models.ForeignKey(Perfil, null=False, blank=False, on_delete=models.CASCADE)
    Fecha = models.DateTimeField(default=timezone.now)
    Beneficios = models.ForeignKey(Beneficios, null=False, blank=False, on_delete=models.CASCADE)

