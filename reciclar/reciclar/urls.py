from django.contrib import admin
from django.urls import include, re_path, path

urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^$', include('appReci.urls'))
]
